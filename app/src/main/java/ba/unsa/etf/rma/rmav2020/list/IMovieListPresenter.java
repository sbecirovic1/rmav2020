package ba.unsa.etf.rma.rmav2020.list;

public interface IMovieListPresenter {

     void searchMovies(String query);
     void getMoviesCursor();
}
