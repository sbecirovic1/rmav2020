package ba.unsa.etf.rma.rmav2020.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Movie implements Parcelable {

    private String title;
    private String overview;
    private String releaseDate;
    private String homepage;
    private String posterPath;
    private Integer id;
    private Integer internalId;
    private String genre;
    private ArrayList<String> actors;
    private ArrayList<String> similarMovies;


    public Integer getInternalId() {
        return internalId;
    }

    public void setInternalId(Integer internalId) {
        this.internalId = internalId;
    }

    public Movie(String title, String overview, String releaseDate, String homepage, String posterPath, Integer id, String genre) {
        this.title = title;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.posterPath = posterPath;
        this.id = id;
        this.genre = genre;
        this.actors = new ArrayList<>();
        this.similarMovies = new ArrayList<>();
    }
    public Movie(String title, String overview, String releaseDate, String homepage, String posterPath, Integer id, String genre, Integer internalId) {
        this.title = title;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.posterPath = posterPath;
        this.id = id;
        this.genre = genre;
        this.internalId = internalId;
        this.actors = new ArrayList<>();
        this.similarMovies = new ArrayList<>();
    }

    public Movie(String title, String overview, String releaseDate, String homepage, String genre, ArrayList<String> actors, ArrayList<String> similarMovies) {
        this.title = title;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.genre = genre;
        this.actors=actors;
        this.similarMovies = similarMovies;
    }
    public Movie(String title, String overview, String releaseDate, String homepage, String genre, ArrayList<String> actors) {
        this.title = title;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.homepage = homepage;
        this.genre = genre;
        this.actors=actors;
    }

    public Movie(Integer id, String title, String overview, String releaseDate, String posterPath) {
        this.id = id;
        this.title = title;
        this.overview = overview;
        this.releaseDate = releaseDate;
        this.posterPath = posterPath;
        this.actors = new ArrayList<>();
        this.similarMovies = new ArrayList<>();
    }

    protected Movie(Parcel in) {
        title = in.readString();
        overview = in.readString();
        releaseDate = in.readString();
        homepage = in.readString();
        posterPath = in.readString();
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            internalId = null;
        } else {
            internalId = in.readInt();
        }
        genre = in.readString();
        actors = in.createStringArrayList();
        similarMovies = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(overview);
        dest.writeString(releaseDate);
        dest.writeString(homepage);
        dest.writeString(posterPath);
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (internalId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(internalId);
        }
        dest.writeString(genre);
        dest.writeStringList(actors);
        dest.writeStringList(similarMovies);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public ArrayList<String> getActors() {
        return actors;
    }

    public void setActors(ArrayList<String> actors) {
        this.actors = actors;
    }

    public ArrayList<String> getSimilarMovies() {
        return similarMovies;
    }

    public void setSimilarMovies(ArrayList<String> similarMovies) {
        this.similarMovies = similarMovies;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public static Creator<Movie> getCREATOR() {
        return CREATOR;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }


}
