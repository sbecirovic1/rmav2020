package ba.unsa.etf.rma.rmav2020.detail;

import android.content.Context;
import android.database.Cursor;

import ba.unsa.etf.rma.rmav2020.data.Movie;

public interface IMovieDetailInteractor {

     void save(Movie movie, Context context);
     Movie getMovie(Context context, Integer id);
     Cursor getCastCursor(Context context, int id);
     Cursor getSimilarCursor(Context context, int id);
}
