package ba.unsa.etf.rma.rmav2020.detail;

public interface IMovieDetailView {

     void refreshView();
     void showToast(String text);
}
