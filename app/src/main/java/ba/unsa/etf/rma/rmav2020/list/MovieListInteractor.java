package ba.unsa.etf.rma.rmav2020.list;

import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.os.ResultReceiver;

import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import ba.unsa.etf.rma.rmav2020.BuildConfig;
import ba.unsa.etf.rma.rmav2020.data.Movie;
import ba.unsa.etf.rma.rmav2020.util.MovieDBOpenHelper;


public class MovieListInteractor extends IntentService implements IMovieListInteractor {

    final public static int STATUS_RUNNING=0;
    final public static int STATUS_FINISHED=1;
    final public static int STATUS_ERROR=2;
    private String tmdb_api_key= BuildConfig.API_KEY;
    ArrayList<Movie> movies;
    private MovieDBOpenHelper movieDBOpenHelper;
    SQLiteDatabase database;
    private Movie movie;

    public MovieListInteractor() {
        super(null);
    };
    public MovieListInteractor(String name) {
        super(name);
    };

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent)  {
        final ResultReceiver receiver = intent.getParcelableExtra("receiver");
        Bundle bundle = new Bundle();
        receiver.send(STATUS_RUNNING, Bundle.EMPTY);
        String params = intent.getStringExtra("query");
        String query = null;
        try {
            query = URLEncoder.encode(params, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "https://api.themoviedb.org/3/search/movie?api_key="+tmdb_api_key+"&query=" + query;
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);
            JSONObject jo = new JSONObject(result);
            JSONArray results = jo.getJSONArray("results");
            movies=new ArrayList<>();
            for (int i = 0; i < results.length(); i++) {
                JSONObject movie = results.getJSONObject(i);
                String title = movie.getString("title");
                Integer id = movie.getInt("id");
                String posterPath = movie.getString("poster_path");
                String overview = movie.getString("overview");
                String releaseDate = movie.getString("release_date");
                movies.add(new Movie(id,title,overview,releaseDate,posterPath));
                if (i==4) break;
            }
        } catch (ClassCastException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        } catch (MalformedURLException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        } catch (IOException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        } catch (JSONException e) {
            bundle.putString(Intent.EXTRA_TEXT, e.toString());
            receiver.send(STATUS_ERROR, bundle);
            return;
        }
        bundle.putParcelableArrayList("result", movies);
        receiver.send(STATUS_FINISHED, bundle);
    }


    @Override
    public Cursor getMovieCursor(Context context) {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                MovieDBOpenHelper.MOVIE_INTERNAL_ID,
                MovieDBOpenHelper.MOVIE_TITLE,
                MovieDBOpenHelper.MOVIE_RELEASEDATE,
                MovieDBOpenHelper.MOVIE_POSTERPATH

        };
        Uri adresa = Uri.parse("content://rma.provider.movies/elements");
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cur = cr.query(adresa,kolone,where,whereArgs,order);
        return cur;
    }



}
