package ba.unsa.etf.rma.rmav2020.list;

import android.content.Context;
import android.database.Cursor;

public interface IMovieListInteractor {

    Cursor getMovieCursor(Context context);

}
