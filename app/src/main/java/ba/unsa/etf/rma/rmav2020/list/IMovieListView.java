package ba.unsa.etf.rma.rmav2020.list;

import android.database.Cursor;

import java.util.ArrayList;

import ba.unsa.etf.rma.rmav2020.data.Movie;

public interface IMovieListView {

    void setMovies(ArrayList<Movie> movies);
    void notifyMovieListDataSetChanged();
    void showToast(String text);

    void setCursor(Cursor cursor);
}
