package ba.unsa.etf.rma.rmav2020.detail;

import android.database.Cursor;
import android.os.Parcelable;

import ba.unsa.etf.rma.rmav2020.data.Movie;

public interface IMovieDetailPresenter {

    void setMovie(Parcelable movie);
    Movie getMovie();
    void searchMovie(String query);
    void getDatabaseMovie(int id);
    Cursor getCastCursor(int id);
    Cursor getSimilarCursor(int id);
}
